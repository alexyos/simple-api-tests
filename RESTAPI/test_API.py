import requests
import json


def js_data():
    with open('data.json', 'r') as file:
        data = json.loads(file.read())
    return data


class TestApi:
    user_id = ""

    def test_new_user(self):
        url = js_data()['cr_user_url']
        headers = js_data()['auth_headers']
        user = js_data()['user']
        response = requests.post(url, headers=headers, json=user)
        print(json.dumps(response.json(), indent=2))
        assert response.json()['code'] == 201, f"Failed to request. Response: " \
                                               f"{response.status_code}"
        TestApi.user_id = response.json()["data"]["id"]
        assert js_data()["user"]["name"] == response.json()["data"]["name"]
        assert js_data()["user"]["email"] == response.json()["data"]["email"]

    def test_update_user(self):
        url = js_data()['upd_user_url'] + str(self.user_id)
        headers = js_data()['auth_headers']
        user = js_data()['user_upd']
        response = requests.patch(url, headers=headers, json=user)
        assert response.status_code == 200, f"Failed to request. Response: " \
                                            f"{response.status_code}"
        assert response.json()["data"]["name"] == js_data()["user_upd"]["name"]
        assert response.json()["data"]["email"] == js_data()["user_upd"][
            "email"]
        print(
            f"code:{response.json()['code']} - User{self.user_id} "
            f"successfully updated")
        print(json.dumps(response.json(), indent=2))

    @staticmethod
    def test_new_user_wrong_auth():
        url = js_data()['cr_user_url']
        headers = js_data()['wrong_token_headers']
        user = js_data()['user']
        response = requests.post(url, headers=headers, json=user)
        assert response.json()['code'] == 401, f"Failed to request. Response: " \
                                               f"{response.status_code}"
        print(f"code:{response.json()['code']} - "
              f"{response.json()['data']['message']}")

    def test_user_del(self):
        url = js_data()['upd_user_url'] + str(self.user_id)
        headers = js_data()['auth_headers']
        response = requests.delete(url, headers=headers)
        assert response.json()['code'] == 204, f"Failed to request. Response: " \
                                               f"{response.status_code}"
        print(
            f"code:{response.json()['code']} - User{self.user_id} successfully "
            f"deleted")
        print(json.dumps(response.json(), indent=2))
